module IntegerInEnglishHelper
  def link_to_sum(number, sum)
    link_to(integer_in_english_path(number + sum)) do
      "#{'+' if sum > 0}#{sum}"
    end
  end
end
