class IntegerInEnglish
  include ActiveModel::Validations

  attr_accessor :number, :recursion_step

  validates_numericality_of :number, only_integer: true, greater_than_or_equal_to: 0
  validates_numericality_of :recursion_step, only_integer: true, greater_than_or_equal_to: 0

  def initialize(number = 0, recursion_step = 0)
    @number = number
    @recursion_step = recursion_step
  end

  def to_s
    string_parts.join(" ").squish
  end

  def number_parts
    if number < 2000 && recursion_step == 0
      [0, number] # cases like 'eleven hundred and one'
                  # and 'nineteen hundred'
    else
      [number / 1000, number % 1000]
    end
  end

  def string_parts
    if valid?
      @number = number.to_i
      [most_significant_part] + if least_significant_part.present?
                                  [least_significant_part, number_unit]
                                else
                                  []
                                end
    end
  end

  def least_significant_part
    @least_significant_part ||= "#{ hundreds } #{ less_than_100 }".squish
  end

  # This method will be responsible for the recursion that will build the string.
  # It will recurse on #number_parts elements.
  def most_significant_part
    most_significant_part_number = number_parts.first
    IntegerInEnglish.new(most_significant_part_number, recursion_step + 1).to_s if most_significant_part_number > 0
  end

  def number_unit
    [nil, "thousand", "million", "billion", "trillion", "quadrillion"][recursion_step]
  end

  def less_than_100
    if number == 0
      "zero"
    else
      one_to_ninety_nine = OneToNinetyNineInEnglish.new(number % 100).to_s
      if number > 100
        one_to_ninety_nine ? "and #{ one_to_ninety_nine }" : nil
      else
        one_to_ninety_nine
      end
    end
  end

  def hundreds
    hundreds_number = number_parts.last / 100
    if hundreds_number > 0 && hundreds_number < 20
      "#{ OneToNinetyNineInEnglish.new(hundreds_number).to_s } hundred"
    end
  end
end
