class OneToNinetyNineInEnglish
  include ActiveModel::Validations

  validates :number, inclusion: { in: 1..99 }

  attr_reader :number

  NUMERALS = [nil, "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"].freeze
  TENS = [nil, "ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"].freeze
  LESS_THAN_20 = [TENS[1], "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"].freeze

  def initialize(number)
    @number = number
  end

  def to_s
    if valid?
      if number < 10
        NUMERALS[number]
      elsif number < 20
        LESS_THAN_20[number - 10]
      else
        [TENS[number / 10],
         NUMERALS[number % 10]].compact.join('-')
      end
    end
  end
end
