class IntegerInEnglishController < ApplicationController
  def show
    @integer_in_english = IntegerInEnglish.new(params[:id])
  end

  def index
    redirect_to integer_in_english_path(params[:integer_in_english].try(:[], :number) || 1)
  end
end
