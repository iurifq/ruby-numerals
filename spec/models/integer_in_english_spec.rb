require 'spec_helper'

describe IntegerInEnglish do
  it { should respond_to :number }
  it { should respond_to :recursion_step }

  context "validation" do
    [:number, :recursion_step].each do |integer_attribute|
      it { should_not allow_value(-1, "string", 1.2).for(integer_attribute) }
      it { should allow_value(1, 200, 0103445, 1000000).for(integer_attribute) }
    end
  end

  describe "#number_parts" do
    context "when the absolute number is less than 2000" do
      subject { IntegerInEnglish.new(1999).number_parts }

      it "puts 'zero' at the beggining" do
        expect(subject.first).to eq 0
      end

      it "puts the rest of the number at the end" do
        expect(subject.last).to eq 1999
      end
    end

    context "when the number is greater or equal to 2000" do
      subject { IntegerInEnglish.new(2000).number_parts }

      it "puts the thousands at the beginning" do
        expect(subject.first).to eq 2
      end

      it "puts the part less than 1000 at the end" do
        expect(subject.last).to eq 0
      end
    end

    context "when the number is less than 2000 but the absolute number greater" do
      subject { IntegerInEnglish.new(1500, 1).number_parts }

      it "puts the thousands at the beginning" do
        expect(subject.first).to eq 1
      end

      it "puts the part less than 1000 at the end" do
        expect(subject.last).to eq 500
      end
    end
  end

  describe "#string_parts" do
    it "is nil if the object is not valid" do
      expect(IntegerInEnglish.new("jflksj").string_parts).to be_nil
    end

    context "when the object is valid" do
      let(:most_significant_part) { "most significant part" }
      let(:least_significant_part) { "least significant part" }
      let(:number_unit) { "number unit" }

      before do
        subject.stub(most_significant_part: most_significant_part)
        subject.stub(number_unit: number_unit)
      end

      it "returns an array with its #most_significant_part if the #least_significant_part is not present" do
        subject.stub(least_significant_part: nil)
        expect(subject.string_parts).to eq [most_significant_part]
      end

      it "returns an array with its #most_significant_part, #least_significant_part and #number_unit" do
        least_significant_part = "least significant part"
        subject.stub(least_significant_part: least_significant_part)
        expect(subject.string_parts).to eq [most_significant_part, least_significant_part, number_unit]
      end
    end
  end

  describe "#most_significant_part" do
    context "when the first element of #number_parts is greater than 0" do
      before do
        subject.stub(number_parts: [1, 0])
      end

      it "builds an IntegerInEnglish instance with the first #number_parts element" do
        IntegerInEnglish.should_receive(:new).with(1, 1)
        subject.most_significant_part
      end

      it "returns the built IntegerInEnglish string representation" do
        result = "resursive call result"
        IntegerInEnglish.stub_chain(:new, :to_s).and_return(result)
        expect(subject.most_significant_part).to eq(result)
      end
    end
  end

  describe "#less_than_100" do
    it "is 'zero' if the number is 0" do
      expect(subject.less_than_100).to eq "zero"
    end

    it "is nil if the part less than 100 of the number is 0" do
      OneToNinetyNineInEnglish.stub_chain(:new, :to_s)
      expect(IntegerInEnglish.new(1100).less_than_100).to be_nil
    end

    context "when the part less than 100 is between 1 and 99" do
      before do
        OneToNinetyNineInEnglish.stub_chain(:new, :to_s).and_return("number to_s")
      end

      context "when the number is greater than 100" do
        it "begins with 'and' if on the first step of the recursion" do
          expect(IntegerInEnglish.new(199).less_than_100).to match /^and /
          expect(IntegerInEnglish.new(101).less_than_100).to match /^and /
        end
      end

      context "when the number is less than 100" do
        it "does not have an 'and'" do
          expect(IntegerInEnglish.new(99, 1).less_than_100).not_to match /and /
          expect(IntegerInEnglish.new(1, 1).less_than_100).not_to match /and /
        end
      end
    end
  end

  describe "#hundreds" do
    before do
      OneToNinetyNineInEnglish.stub(:new)
      OneToNinetyNineInEnglish.stub(:new).with(1).and_return(one_to_ninety_1)
      OneToNinetyNineInEnglish.stub(:new).with(19).and_return(one_to_ninety_19)
      OneToNinetyNineInEnglish.stub(:new).with(20).and_return(one_to_ninety_20)
    end

    let(:one_to_ninety_1) do
      one_to_ninety_1 = double("one to ninety nine of 1")
      one_to_ninety_1.stub(to_s: "one")
      one_to_ninety_1
    end

    let(:one_to_ninety_19) do
      one_to_ninety_19 = double("one to ninety nine of 19")
      one_to_ninety_19.stub(to_s: "nineteen")
      one_to_ninety_19
    end

    let(:one_to_ninety_20) do
      one_to_ninety_20 = double("one to ninety nine of 20")
      one_to_ninety_20.stub(to_s: "twenty")
      one_to_ninety_20
    end

    it "deals with cases like 'one hundred'" do
      subject.stub(number_parts: [1, 100])
      expect(subject.hundreds).to eq "one hundred"
    end

    it "deals with cases like 'nineteen hundred'" do
      subject.stub(number_parts: [1, 1900])
      expect(subject.hundreds).to eq "nineteen hundred"
    end

    it "does not allow hundreds greater or equals to 20" do
      subject.stub(number_parts: [1, 2000])
      expect(subject.hundreds).to be_nil
    end
  end

  context "integration tests" do
    describe "#to_s" do
      def integer_to_english_is(number, number_string)
        expect(IntegerInEnglish.new(number).to_s).to eq number_string
      end

      it "works for numbers between 1..99" do
        integer_to_english_is(0, "zero")
        integer_to_english_is(1, "one")
        integer_to_english_is(10, "ten")
        integer_to_english_is(31, "thirty-one")
        integer_to_english_is(50, "fifty")
      end

      it "works for numbers between 100..999" do
        integer_to_english_is(100, "one hundred")
        integer_to_english_is(201, "two hundred and one")
        integer_to_english_is(999, "nine hundred and ninety-nine")
      end

      it "works for numbers between 1000..1999" do
        integer_to_english_is(1000, "ten hundred")
        integer_to_english_is(1200, "twelve hundred")
        integer_to_english_is(1999, "nineteen hundred and ninety-nine")
      end

      it "works for numbers between 2000..9999" do
        integer_to_english_is(2000, "two thousand")
        integer_to_english_is(2222, "two thousand two hundred and twenty-two")
        integer_to_english_is(9999, "nine thousand nine hundred and ninety-nine")
      end

      it "works for big numbers" do
        integer_to_english_is(200076, "two hundred thousand and seventy-six")
        integer_to_english_is(1200076, "one million two hundred thousand and seventy-six")
        integer_to_english_is(10200076, "ten million two hundred thousand and seventy-six")
        integer_to_english_is(250200076, "two hundred and fifty million two hundred thousand and seventy-six")
      end
    end
  end
end
