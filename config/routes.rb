RubyNumerals::Application.routes.draw do
  resources :integer_in_english, only: [:show, :index]
end
